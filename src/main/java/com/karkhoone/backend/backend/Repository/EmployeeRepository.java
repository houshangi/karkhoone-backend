package com.karkhoone.backend.backend.Repository;

import com.karkhoone.backend.backend.Domain.Employee;
import org.springframework.data.repository.CrudRepository;

public interface EmployeeRepository extends CrudRepository<Employee, Integer> {
    Iterable<Employee> findByCity(String city);
 }

