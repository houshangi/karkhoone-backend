package com.karkhoone.backend.backend;

import com.karkhoone.backend.backend.Repository.EmployeeRepository;
import com.karkhoone.backend.backend.RestController.EmployeeController;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.request;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(EmployeeController.class)
public class BackendApplicationTests {

	@Autowired
	private MockMvc mockMvc;

	@MockBean
	EmployeeRepository employeeRepository;


	@Test
	public void contextLoads() throws Exception {

		mockMvc.perform(get("/employee/add?name=test&city=tehran&field=dude&code=1234")
				.contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk());
	}

	@Test
	public void showall() throws Exception{
		mockMvc.perform(get("/employee/all"))
				.andExpect(status().isOk());

	}

}
